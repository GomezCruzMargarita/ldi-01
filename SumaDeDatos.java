/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Programas;

import java.util.Scanner;

/**
 *
 * @author Margarita
 */
public class SumaDeDatos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
       int opcionp;
       
       Scanner entrada = new Scanner(System.in);
       
       do{
       System.out.println(".:: SUMAS ::.");
       System.out.println("1.- BYTE");
       System.out.println("2.- WORD");
       System.out.println("3.- DWORD");
       System.out.println("4.- QWORD");
       System.out.println("5.- SALIR");
       opcionp=entrada.nextInt();
       
       switch(opcionp){
       
    case 1:
      
       String opByte;
       
       do
       {
       System.out.println(".:: BYTE ::.");
       System.out.println("a) Operacion BYTE + BYTE");
       System.out.println("b) Operacion BYTE + WORD");
       System.out.println("c) Operacion BYTE + DWORD");
       System.out.println("d) Operacion BYTE + QWORD");
       System.out.println("r) Menu Inicial");
       opByte=entrada.next();
       
       switch(opByte){
           case "a":
                String byte1;
                String byte2;
                 
                System.out.println("Ingresa el primer numero en byte a sumar:");
                byte1=entrada.next();
                System.out.println("Ingresa el segundo numero en byte a sumar:");
                byte2=entrada.next();
        
                if (byte1.length() == 8 && byte2.length() == 8)
                {
             
                byte dato1 = Byte.parseByte(byte1, 2);
                byte dato2 = Byte.parseByte(byte2, 2);
                byte sum = (byte) (dato1 + dato2);
      
                String res = Integer.toString(sum, 2);
                int longitud = res.length();
                int caracteres = 8 - longitud;
                
                System.out.println("El resultado de la suma de byte es: ");
        
                for (int i=0; i<caracteres; i++)
                {
                System.out.print('0');
                }
        
                System.out.print(res);
                System.out.println(" ");
                }
                
                else
                {
                System.err.println("Alguno de los numeros no tiene 8 bytes de tamaño, verifica tus datos");
                }
                break;
               
               case "b":
                String vbyte1;
                String vword1;
               
                System.out.println("Ingresa el primer numero en byte a sumar:");
                vbyte1=entrada.next();
                System.out.println("Ingresa el segundo numero en word a sumar:");
                vword1=entrada.next();
        
                if (vbyte1.length() == 8 && vword1.length() == 16)
                {
             
                byte dato1 = Byte.parseByte(vbyte1, 2);
                long dato2 = Long.parseLong(vword1, 2);
                long sum = (long) (dato1 + dato2);
      
                String res = Long.toString(sum, 2);
                int longitud = res.length();
                int caracteres = 16 - longitud;
                
                System.out.println("El resultado de la suma entre un byte y un word es: ");
                for (int i=0; i<caracteres; i++)
                {
                System.out.print('0');
                }
        
                System.out.print(res);
                System.out.println(" ");
                }
                
                else
                {
                System.err.println("Alguno de los numeros no tiene 16 bytes de tamaño, verifica tus datos");
                }
                break;
                   
               case "c":
                String bytesum2;
                String vdword2;
               
                System.out.println("Ingresa el primer numero en byte a sumar:");
                bytesum2=entrada.next();
                System.out.println("Ingresa el segundo numero en double word a sumar:");
                vdword2=entrada.next();
       
                if (bytesum2.length() == 8 && vdword2.length() == 32)
                {
             
                byte dato1 = Byte.parseByte(bytesum2, 2);
                long dato2 = Long.parseLong(vdword2, 2);
                long sum = (long) (dato1 + dato2);
      
                String resultado = Long.toString(sum, 2);
                int longitud = resultado.length();
                int caracteres = 32 - longitud;
                
                System.out.println("El resultado de la suma entre un byte y un double word es: ");
        
                for (int i=0; i<caracteres; i++)
                {
                System.out.print('0');
                }
        
                System.out.print(resultado);
                System.out.println(" ");
                }
                
                else
                {
                System.err.println("Alguno de los numeros no tiene 32 bytes de tamaño, verifica tus datos");
                }
                break;
                   
            case "d":
                String bytesum3;
                String qword3;
               
                System.out.println("Ingresa el primer numero en byte a sumar:");
                bytesum3=entrada.next();
                System.out.println("Ingresa el segundo numero en quad word a sumar:");
                qword3=entrada.next();
       
                if (bytesum3.length() == 8 && qword3.length() == 64)
                {
             
                byte dato1 = Byte.parseByte(bytesum3, 2);
                long dato2 = Long.parseLong(qword3, 2);
                long sum = (long) (dato1 + dato2);
      
                String resultado = Long.toString(sum, 2);
                int longitud = resultado.length();
                int caracteres = 64 - longitud;
                
                System.out.println("El resultado de la suma entre un byte y un quad word es: ");
        
                for (int i=0; i<caracteres; i++)
                {
                System.out.print('0');
                }
        
                System.out.print(resultado);
                System.out.println(" ");
                }
                
                else
                {
                System.err.println("Alguno de los numeros no tiene 64 bytes de tamaño, verifica tus datos");
                }
                break;
       }
       }while(!"r".equals(opByte));
    break;
    
    case 2:
       String opWord;
       do{
       System.out.println(".:: WORD ::.");
       System.out.println("a) Operacion WORD + WORD");
       System.out.println("b) Operacion WORD + BYTE");
       System.out.println("c) Operacion WORD + DWORD");
       System.out.println("d) Operacion WORD + QWORD");
       System.out.println("r) Menu Inicial");
       opWord=entrada.next();
       
       switch(opWord){
        
        case "a":
        String vword1;
        String vword2;
       
        System.out.println("Ingresa el primer valor word para sumar:");
        vword1=entrada.next();
        System.out.println("Ingresa la segunda word para sumar:");
        vword2=entrada.next();
        
        if (vword1.length() == 16 && vword2.length() == 16){
             
        long dato1 = Long.parseLong(vword1,2);
        long dato2 = Long.parseLong(vword2,2);
        long sum = (long) (dato1 + dato2);
      
        String res = Long.toString(sum, 2);
        int longitud = res.length();
        int caracteres = 16 - longitud;
        System.out.println("El resultado de la suma de word es: ");
        
        for (int i=0; i<caracteres; i++){
            System.out.print('0');
        }
        
        System.out.print(res);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 16 bytes de tamaño, verifica tus datos");
        }
        break;
         
        case "b":
        String sumword1;
        String vbyte;
      
        System.out.println("Ingresa el primer valor word para sumar:");
        sumword1=entrada.next();
        System.out.println("Ingresa el segundo valor byte para sumar:");
        vbyte=entrada.next();
        
        if (sumword1.length() == 16 && vbyte.length() == 8){
             
        long dato1 = Long.parseLong(sumword1,2);
        long dato2 = Long.parseLong(vbyte,2);
        long sum = (long) (dato1 + dato2);
      
        String res = Long.toString(sum, 2);
        int longitud = res.length();
        int caracteres = 16 - longitud;
        System.out.println("El resultado de la suma de un word y un byte es: ");
        
        for (int i=0; i<caracteres; i++){
            System.out.print('0');
        }
        
        System.out.print(res);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 16 bytes de tamaño, verifica tus datos");
        }
        break;
        
        case "c":
        String sumword2;
        String dword;
       
        System.out.println("Ingresa el primer valor word para sumar:");
        sumword2=entrada.next();
        System.out.println("Ingresa el segundo valor double word para sumar:");
        dword=entrada.next();
        
        if (sumword2.length() == 16 && dword.length() == 32){
             
        long dato1 = Long.parseLong(sumword2,2);
        long dato2 = Long.parseLong(dword,2);
        long suma = (long) (dato1 + dato2);
      
        String resultado = Long.toString(suma, 2);
        int longitud = resultado.length();
        int caracteres = 32 - longitud;
        System.out.println("El resultado de la suma de un word y un double word es: ");
        
        for (int i=0; i<caracteres; i++){
            System.out.print('0');
        }
        
        System.out.print(resultado);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 16 bytes de tamaño, verifica tus datos");
        }
        break;
        
        case "d":
        String sumword3;
        String qword;
  
        System.out.println("Ingresa el primer valor word para sumar:");
        sumword3=entrada.next();
        System.out.println("Ingresa el segundo valor quad word para sumar:");
        qword=entrada.next();
        
        if (sumword3.length() == 16 && qword.length() == 64){
             
        long dato1 = Long.parseLong(sumword3,2);
        long dato2 = Long.parseLong(qword,2);
        long suma = (long) (dato1 + dato2);
      
        String res = Long.toString(suma, 2);
        int longitud = res.length();
        int caracteres = 64 - longitud;
        System.out.println("El resultado de la suma de un word y un quad word es: ");
        
        for (int i=0; i<caracteres; i++){
            System.out.print('0');
        }
        
        System.out.print(res);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 16 bytes de tamaño, verifica tus datos");
        }
        break;
               
       }
       }while(!"r".equals(opWord));
       break;
     
        
    case 3:
        String opDword;
       do{
       System.out.println("--- OPERACIONES CON DWORD ---");
       System.out.println("a)Suma: DWORD + DWORD");
       System.out.println("b)Suma: DWORD + BYTE");
       System.out.println("c)Suma: DWORD + WORD");
       System.out.println("d)Suma: DWORD + QWORD");
       System.out.println("r) Regresar al menu principal");
       opDword=entrada.next();
       
       switch(opDword){
        
        case "a": 
        String dword1;
        String dword2;
    
        System.out.println("Ingresa el primer double word para sumar:");
        dword1=entrada.next();
        System.out.println("Ingresa el segundo double word para sumar:");
        dword2=entrada.next();
       
        if (dword1.length() == 32 && dword2.length() == 32){
             
        long dato1 = Long.parseLong(dword1,2);
        long dato2 = Long.parseLong(dword2,2);
        long sum = (long) (dato1 + dato2);
      
        String resultado = Long.toString(sum, 2);
        int longitud = resultado.length();
        int caracteres = 32 - longitud;
        System.out.println("El resultado de la suma de double word es: ");
        
        for (int i=0; i<caracteres; i++){
            System.out.print('0');
        }
        
        System.out.print(resultado);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 32 bytes de tamaño, verifica tus datos");
        }
        break;
            
        case "b": 
        String sumdword2;
        String byte1;
    
        System.out.println("Ingresa el primer valor double word para sumar:");
        sumdword2=entrada.next();
        System.out.println("Ingresa el segundo valor byte para sumar:");
        byte1=entrada.next();
       
        if (sumdword2.length() == 32 && byte1.length() == 8){
             
        long dato1 = Long.parseLong(sumdword2,2);
        long dato2 = Long.parseLong(byte1,2);
        long suma = (long) (dato1 + dato2);
      
        String resultado = Long.toString(suma, 2);
        int longitud = resultado.length();
        int caracteres = 32 - longitud;
        System.out.println("El resultado de la suma de double word y byte es: ");
        
        for (int i=0; i<caracteres; i++){
            System.out.print('0');
        }
        
        System.out.print(resultado);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 32 bytes de tamaño, verifica tus datos");
        }
        break;
        
        case "c": 
        String dwordsum2;
        String word2;
    
        System.out.println("Ingresa el primer valor double word para sumar:");
        dwordsum2=entrada.next();
        System.out.println("Ingresa el segundo valor word para sumar:");
        word2=entrada.next();
       
        if (dwordsum2.length() == 32 && word2.length() == 16){
             
        long dato1 = Long.parseLong(dwordsum2,2);
        long dato2 = Long.parseLong(word2,2);
        long suma = (long) (dato1 + dato2);
      
        String resultado = Long.toString(suma, 2);
        int longitud = resultado.length();
        int caracteres = 32 - longitud;
        System.out.println("El resultado de la suma de double word y word es: ");
        
        for (int i=0; i<caracteres; i++){
            System.out.print('0');
        }
        
        System.out.print(resultado);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 32 bytes de tamaño, verifica tus datos");
        }
        break;
            
         case "d": 
        String dwordsum3;
        String qword3;
    
        System.out.println("Ingresa el primer valor double word para sumar:");
        dwordsum3=entrada.next();
        System.out.println("Ingresa el segundo valor quad word para sumar:");
        qword3=entrada.next();
       
        if (dwordsum3.length() == 32 && qword3.length() == 64){
             
        long dato1 = Long.parseLong(dwordsum3,2);
        long dato2 = Long.parseLong(qword3,2);
        long suma = (long) (dato1 + dato2);
      
        String resultado = Long.toString(suma, 2);
        int longitud = resultado.length();
        int caracteres = 64 - longitud;
        System.out.println("El resultado de la suma de double word y quad word es: ");
        
        for (int i=0; i<caracteres; i++){
            System.out.print('0');
        }
        
        System.out.print(resultado);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 64 bytes de tamaño, verifica tus datos");
        }
        break;
             
       }
     }while(!"r".equals(opDword));
       break;
        
    case 4:
        
       String opQword;
       do{
       System.out.println(".:: QWORD ::.");
       System.out.println("a) Operacion QWORD + QWORD");
       System.out.println("b) Operacion QWORD + BYTE");
       System.out.println("c) Operacion QWORD + WORD");
       System.out.println("d) Operacion QWORD + DWORD");
       System.out.println("r) Menu Inicial");
       opQword=entrada.next();
       
       switch(opQword){
        
       case "a":
       String qword1;
       String qword2;
        
        System.out.println("Ingresa el primer quad word para sumar:");
        qword1=entrada.next();
        System.out.println("Ingresa el segundo quad word para sumar:");
        qword2=entrada.next();
       
        if (qword1.length() == 64 && qword2.length() == 64){
             
        long dato1 = Short.parseShort(qword1,2);
        long dato2 = Short.parseShort(qword2,2);
        long suma = (long) (dato1 + dato2);
      
        String resultado = Long.toString(suma, 2);
        int longitud = resultado.length();
        int caracteres = 64 - longitud;
        System.out.println("El resultado de la suma de quad word es: ");
        
        for (int i=0; i<caracteres; i++){
            System.out.print('0');
        }
        
        System.out.print(resultado);
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 64 bytes de tamaño, verifica tus datos");
        }
        break;
        
        case "b":
       String qwordsum1;
       String byte1;
        
        System.out.println("Ingresa el primer valor quad word para sumar:");
        qwordsum1=entrada.next();
        System.out.println("Ingresa el segundo valor byte para sumar:");
        byte1=entrada.next();
       
        if (qwordsum1.length() == 64 && byte1.length() == 8){
             
        long dato1 = Long.parseLong(qwordsum1,2);
        long dato2 = Long.parseLong(byte1,2);
        long sum = (long) (dato1 + dato2);
      
        String resultado = Long.toString(sum, 2);
        int longitud = resultado.length();
        int caracteres = 64 - longitud;
        System.out.println("El resultado de la suma entre quad word y byte es: ");
        
        for (int i=0; i<caracteres; i++){
            System.out.print('0');
        }
        
        System.out.print(resultado);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 64 bytes de tamaño, verifica tus datos");
        }
        break;
            
       case "c":
       String qwordsum2;
       String word2;
        
        System.out.println("Ingresa el primer valor quad word para sumar:");
        qwordsum2=entrada.next();
        System.out.println("Ingresa el segundo valor word para sumar:");
        word2=entrada.next();
       
        if (qwordsum2.length() == 64 && word2.length() == 16){
             
        long dato1 = Long.parseLong(qwordsum2,2);
        long dato2 = Long.parseLong(word2,2);
        long suma = (long) (dato1 + dato2);
      
        String resultado = Long.toString(suma, 2);
        int longitud = resultado.length();
        int caracteres = 64 - longitud;
        System.out.println("El resultado de la suma entre quad word y word es: ");
        
        for (int i=0; i<caracteres; i++){
            System.out.print('0');
        }
        
        System.out.print(resultado);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 64 bytes de tamaño, verifica tus datos");
        }
        break;    
           
            case "d":
       String qwordsum3;
       String dword3;
        
        System.out.println("Ingresa el primer valor quad word para sumar:");
        qwordsum3=entrada.next();
        System.out.println("Ingresa el segundo valor double word para sumar:");
        dword3=entrada.next();
       
        if (qwordsum3.length() == 64 && dword3.length() == 32){
             
        long dato1 = Long.parseLong(qwordsum3,2);
        long dato2 = Long.parseLong(dword3,2);
        long suma = (long) (dato1 + dato2);
      
        String resultado = Long.toString(suma, 2);
        int longitud = resultado.length();
        int caracter = 64 - longitud;
        System.out.println("El resultado de la suma entre quad word y double word es: ");
        
        for (int i=0; i<caracter; i++){
            System.out.print('0');
        }
        
        System.out.print(resultado);
        System.out.println(" ");
            }
        else
        {
            System.err.println("Alguno de los numeros no tiene 64 bytes de tamaño, verifica tus datos");
        }
        break;        
       }
       }while(!"r".equals(opQword));
        break;
    }
}while(opcionp != 5);
    }
}