/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Programas;

import java.util.Scanner;

/**
 *
 * @author Margarita
 */
public class BinarioDecimal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String binario;//Variable que almacena el numero binario
        Scanner leer = new Scanner(System.in);
        System.out.println("Ingresa el numero binario a convetir a decimal:");
        binario=leer.next();
        
        long numbinario = Long.parseLong(binario); //convertimos el string leido a long
        long numdecimal = 0; //almacena el decimal resultante
        int contador = 1; //Contador se inicializa en 1
        long auxdecimal; 
        
        while (numbinario > 0){
            auxdecimal = numbinario % 2; //auxdecimal tomara el restante de numbinario sobre modulo 2
            numdecimal = numdecimal + auxdecimal * contador;
            numbinario = numbinario / 10; // se divide entre 10 debido a la base decimal
            contador = contador * 2;
        }
        System.out.println("El numero binario "+binario+" en decimal es: " +numdecimal);
    }
    }
    

