/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Programas;

import java.util.Scanner;

/**
 *
 * @author Margarita
 */
public class SumaBinaria {

    /**
     * @param args the command line arguments
     */
    
    
    public static void main(String[] args) {
        // TODO code application logic here
        String binario1;
        String binario2;
        Scanner leer = new Scanner(System.in);
        System.out.println("Ingresa el primer numero binario a sumar:");
        binario1=leer.next();
        System.out.println("Ingresa el segundo numero binario a sumar:");
        binario2=leer.next();
        
        int num1 = Integer.parseInt(binario1, 2);
        int num2 = Integer.parseInt(binario2, 2);
        int res = num1 + num2;
        String resultado = Integer.toString(res, 2);
        System.out.println("El resultado de la suma es: " +resultado);
    }
    
}
