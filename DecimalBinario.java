/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Programas;

import java.util.Scanner;

/**
 *
 * @author Margarita
 */
public class DecimalBinario {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        long numero;
        Scanner leer = new Scanner(System.in);
        System.out.println("Ingresa el numero decial a convetir a binario: ");
        numero=leer.nextLong();
         
        //Numero leido por el susario
        long aux = numero; //aux toma el valor de numero
        String binario="";//Variable que almacena el numero binario
        
        while (aux > 0){
            binario = aux % 2 + binario; //Asigna los valores de unos y ceros   
            aux = aux /2; //se divide el numero decimal entre de dos
        }
        System.out.println("El numero decimal "+numero+" en binario es: " +binario);
    }
    
}
